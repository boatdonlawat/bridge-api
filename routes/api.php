<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('API')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::post('authenticate', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('registration', 'AuthController@registration');
    });
    Route::prefix('stores')->group(function () {
        Route::get('/', 'StoreController@getStores');
        Route::patch('{id}/bookmarks', 'StoreController@addBookmarks');
    });
});
