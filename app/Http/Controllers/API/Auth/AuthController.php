<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $url = $this->owndaysUrl . 'authenticate';
        $response = $this->POST($request, $url);
        if ($response->code === 200) {
            return Resource::make(collect($response->data));
        } else {
            return response()->json($response->data, $response->code);
        }
    }

    public function logout(Request $request)
    {
        $url = $this->owndaysUrl . 'logout';
        $response = $this->POST($request, $url);
        if ($response->code === 200) {
            return Resource::make(collect($response->data));
        } else {
            return response()->json($response->data, $response->code);
        }
    }

    public function registration(Request $request)
    {
        $url = $this->owndaysUrl . 'registration';
        $response = $this->POST($request, $url);
        if ($response->code === 200) {
            return Resource::make(collect($response->data));
        } else {
            return response()->json($response->data, $response->code);
        }
    }
}
