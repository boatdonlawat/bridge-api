<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\Resource;

class StoreController extends Controller
{

    public function getStores(Request $request)
    {
        $url = $this->owndaysUrl . 'stores';
        $response = $this->GET($request, $url);
        if ($response->code === 200) {
            return Resource::make($response->data);
        } else {
            return response()->json($response->data, $response->code);
        }
    }

    public function addBookmarks($id, Request $request)
    {
        $url = $this->owndaysUrl . 'stores' . '/' . $id . '/bookmarks';
        $response = $this->PATCH($request, $url);
        return response()->json($response->data, $response->code);
    }
}
