<?php

namespace App\Http\Controllers;

use ErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $client;
    protected $owndaysUrl;

    public function __construct()
    {
        $this->client = new Client();
        $this->owndaysUrl = env('OWNDAYS_API');
    }

    protected function GET($request, $url)
    {
        return $this->request($request, $url. '?' . $request->getQueryString(), 'GET');
    }

    protected function POST($request, $url)
    {
        return $this->request($request, $url, 'POST');
    }

    protected function DELETE($request, $url)
    {
        return $this->request($request, $url, 'DELETE');
    }
    protected function PUT($request, $url)
    {
        return $this->request($request, $url, 'PUT');
    }
    protected function PATCH($request, $url)
    {
        return $this->request($request, $url, 'PATCH');
    }

    protected function request($request, $url, $method)
    {
        try {
            $token = $request->bearerToken();
            $formData = $request->all() ?: null;
            $response = $this->client->request($method, $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
                'form_params' => $formData
            ]);
            return (object) ['data' => json_decode($response->getBody()), 'code' => 200];
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            $jsonBody = (string) $response->getBody();
            return (object) [ 'data' => json_decode($jsonBody, true), 'code' => $response->getStatusCode()];
        }
    }
}
